<?php

namespace Mgo\CustomFieldsBundle\DependencyInjection\Compiler;

use Mgo\CustomFieldsBundle\Form\CustomFieldsFormTrait;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Ce compiler pass sert a call la methode setCustomFieldsService
 * sur les services form avec le tag form.type.
 */
class FormPass implements CompilerPassInterface
{
    const TAG_NAME = 'form.type'; // tag service symfony pour les formulaire
    const METHOD_NAME = 'setCustomFieldsService'; // nom de la methode a appeler
    const SERVICE_NAME = 'mgo_custom_fields.form_fields'; // nom du service a injecter

    /**
     * {@inheritDoc}
     *
     * @see \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface::process()
     */
    public function process(ContainerBuilder $container)
    {
        foreach ($container->findTaggedServiceIds(self::TAG_NAME) as $name => $data) {
            $definition = $container->getDefinition($name);
            $className = $definition->getClass();
            if ($this->useCustomFieldsTrait($className)) {
                $definition->addMethodCall(
                    self::METHOD_NAME,
                    [new Reference(self::SERVICE_NAME)]
                );
            }
        }
    }

    /**
     * Check si une classe utilise le CustomFieldsTrait.
     */
    private function useCustomFieldsTrait(string $className): bool
    {
        if (class_exists($className)) {
            $traits = \class_uses($className);
            $parents = array_merge(
                [$className],
                (array) class_parents($className)
            );
            foreach ($parents as $parent) {
                $traits = \class_uses($parent);
                if (isset($traits[CustomFieldsFormTrait::class])) {
                    return true;
                }
            }
        }

        return false;
    }
}
