<?php

namespace Mgo\CustomFieldsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgo_custom_fields');
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('mgo_sync_entity');
        }

        $rootNode
            ->children()
                ->arrayNode('entities')
                    ->useAttributeAsKey('entity')
                    ->arrayPrototype()
                        ->useAttributeAsKey('field_name')
                        ->arrayPrototype()
                            ->ignoreExtraKeys(false) // TODO delete when definition is finished
                            ->children()
                                ->enumNode('field_type')
                                    ->values(['text', 'select', 'date', 'entity'])
                                ->end()
                                ->scalarNode('field_label')
                                    ->defaultNull()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('access_rights_table')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
