<?php

namespace Mgo\CustomFieldsBundle\EntityHelper;

/**
 * Maps form classes to customField classes.
 */
class EntityMapper
{
    /**
     * @var string[] custom field classes, with their base form class as key
     *
     * When a custom field class has multiple keys, the first one is used in @{see getFormClass()}
     */
    // phpcs:disable
    private static $map = [
        'Symfony\Component\Form\Extension\Core\Type\TextType' => 'Mgo\CustomFieldsBundle\Entity\TextCustomField',
        'FOS\CKEditorBundle\Form\Type\CKEditorType' => 'Mgo\CustomFieldsBundle\Entity\TextareaCustomField',
         // we have two mapping for TextareaCustomField. the second (default textarea) is used only in "getCustomFieldClass"
        'Symfony\Component\Form\Extension\Core\Type\TextareaType' => 'Mgo\CustomFieldsBundle\Entity\TextareaCustomField',
        'Symfony\Component\Form\Extension\Core\Type\DateTimeType' => 'Mgo\CustomFieldsBundle\Entity\DatetimeCustomField',
        'Symfony\Component\Form\Extension\Core\Type\DateType' => 'Mgo\CustomFieldsBundle\Entity\DatetimeCustomField',
        'Tetranz\Select2EntityBundle\Form\Type\Select2EntityType' => 'Mgo\CustomFieldsBundle\Entity\EntityCustomField',
        'Symfony\Bridge\Doctrine\Form\Type\EntityType' => 'Mgo\CustomFieldsBundle\Entity\EntityCustomField',
    ];
    // phpcs:enable

    /**
     * Get the custom field class for the form class.
     *
     * @param string $formClass
     *
     * @return string custom field class
     */
    public static function getCustomFieldClass($formClass)
    {
        $tryFormClass = $formClass;
        while ($tryFormClass) {
            if (array_key_exists($tryFormClass, self::$map)) {
                return self::$map[$tryFormClass];
            } elseif (class_exists($tryFormClass)) {
                $tryFormClass = (new $tryFormClass())->getParent();
            } else {
                break; // done, unexisting class
            }
        }

        throw new \LogicException(sprintf('FormClass %s is not supported', $formClass));
    }

    /**
     * Gets the base form class for the custom field class.
     *
     * When there are sevaral base classes in @{see self::$map},
     * the first matching is returned.
     *
     * @param string $customFieldClass
     *
     * @return string form class for $customFieldClass
     */
    public static function getFormClass($customFieldClass)
    {
        $key = array_search($customFieldClass, self::$map, true);
        if (false !== $key) {
            return $key;
        } else {
            throw new \LogicException(sprintf('CustomFieldClass %s is not supported.', $customFieldClass));
        }
    }

    public static function isEntityField($formClass)
    {
        if ('Symfony\Bridge\Doctrine\Form\Type\EntityType' == $formClass
            || 'Tetranz\Select2EntityBundle\Form\Type\Select2EntityType' == $formClass
        ) {
            return true;
        } else {
            return false;
        }
    }
}
