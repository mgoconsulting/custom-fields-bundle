<?php

namespace Mgo\CustomFieldsBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;

trait CustomFieldsFormTrait
{
    /** @var CustomFieldsFormService */
    protected $customFieldsService;

    public function setCustomFieldsService(CustomFieldsFormService $customFieldsService)
    {
        $this->customFieldsService = $customFieldsService;
    }

    protected function initCustomFieldsService(FormBuilderInterface $builder, array $options)
    {
        if ($this->customFieldsService) {
            $this->customFieldsService->addCustomFields(
                $builder,
                ($options['data_class'] ?? null)
            );
        }
    }
}
