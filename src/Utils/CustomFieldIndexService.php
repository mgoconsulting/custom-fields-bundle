<?php

namespace Mgo\CustomFieldsBundle\Utils;

use Symfony\Component\Form\Form;

/**
 * This service allows to get required information from custom_fields table for building up index pages
 * of entities linked to custom fields.
 */
class CustomFieldIndexService
{
    private $configReader;

    public function __construct(ConfigReader $configReader)
    {
        $this->configReader = $configReader;
    }

    /**
     * Retrieves the values of the custom fields for a given entity according to a given form fields set (filterform)
     * Values are returned together with a raw parameter (boolean) indicating whether the raw filter
     * should be applied during print out.
     *
     * @param Form   $filterform filter form containing all form fields which shall be available for filtering
     * @param object $entity     An entity stored in the database
     *
     * @return array Contains the values of all custom fields which are referenced in the entity
     *               for all types contained in filterform, together with 'raw' indicator
     */
    public function getIndexRows(Form $filterform, $entity)
    {
        $fieldConfig = $this->configReader->getConfigForEntity($entity);
        $indexRows = [];
        // iterate over filterform custom fields
        foreach ($filterform as $filterfield) {
            if ('custom_fields' == $filterfield->getConfig()->getOption('translation_domain')) {
                $fieldGetter = $filterfield->getName();
                // we can access the customField directly by its name
                // (using the magic getter function in the custom field trait)
                $value = $entity->$fieldGetter;
                $raw = false;
                if (array_key_exists($fieldGetter, $fieldConfig)
                    && 'FOS\CKEditorBundle\Form\Type\CKEditorType' == $fieldConfig[$fieldGetter]['type']) {
                    $raw = true;
                }
                $indexRows[] = [
                    'value' => $value,
                    'raw' => $raw,
                ];
            }
        }

        return $indexRows;
    }

    /**
     * Retrieves the header data of the custom fields for a given form fields set (filterform)
     * Values are returned together with a raw parameter (boolean)
     * indicating whether the raw filter should be applied during print out.
     *
     * @param array $options
     *
     * @return array
     */
    public function getHeader(Form $filterform, $options = [])
    {
        $header = [];
        foreach ($filterform as $filterfield) {
            if ('custom_fields' == $filterfield->getConfig()->getOption('translation_domain')) {
                $headerElem = ['name', 'class', 'label'];
                foreach ($options as $optionKey => $option) {
                    $headerElem[$optionKey] = $option;
                }
                $headerElem['name'] = $filterfield->getName();
                $headerElem['class'] = $filterfield->getName().'Col';
                $headerElem['label'] = $filterfield->getConfig()->getOption('label');

                $header[] = $headerElem;
            }
        }

        return $header;
    }
}
