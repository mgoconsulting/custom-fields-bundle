<?php

namespace Tests\EntityHelper;

use Mgo\CustomFieldsBundle\CustomFieldsEntityTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CustomFieldsTestBase extends TestCase
{
    const MOCK_ENTITY_CLASS = 'Mock_GetTestSetEntity';

    public function tearDown(): void
    {
        $this->unsetTestConfig();
    }

    protected function setTestConfig()
    {
        global $kernel;
        if (!$kernel || 'M' === get_class($kernel)[0]) { // kernel is not set or is Mocked class
            // create mocked container in mocked kernel for UnsavedCustomField
            $config = [
                'checkCorrectEntityFinding1' => [
                    'aDateTimeField' => ['type' => 'invalid 1'],
                ],
                self::MOCK_ENTITY_CLASS => [
                    'notYetExisting' => ['type' => TextType::class],
                    'aDateTimeField' => ['type' => DateTimeType::class],
                    'someEntityType' => ['type' => EntityType::class],
                ],
                'checkCorrectEntityFinding2' => [
                    'aDateTimeField' => ['type' => 'also not valid'],
                ],
            ];

            $mockContainer = $this->getMockBuilder('dummy\Container')
                ->disableAutoload()
                ->setMethods(['getParameter'])
                ->getMock();
            $mockContainer->expects($this->any())->method('getParameter')->will($this->returnValue($config));
            $mockKernel = $this->getMockBuilder('dummy\Kernel')
                ->disableAutoload()
                ->setMethods(['getContainer'])
                ->getMock();
            $mockKernel
                ->expects($this->atLeastOnce()) // remove generating $kernel when not needed anymore
                ->method('getContainer')
                ->will($this->returnValue($mockContainer));
            $kernel = $mockKernel;
        }
    }

    protected function unsetTestConfig()
    {
        global $kernel;
        static $mockNoKernel = null;

        if ($kernel && (is_null($mockNoKernel) || $kernel instanceof $mockNoKernel)) {
            if (is_null($mockNoKernel)) {
                $mockNoKernel = $this->getMockBuilder('dummy\NoKernel')->disableAutoLoad()->getMock();
            }
            $kernel = $mockNoKernel;
        }
    }

    protected function getMockEntity()
    {
        $entity = $this->getMockForTrait(CustomFieldsEntityTrait::class, [], self::MOCK_ENTITY_CLASS);

        return $entity;
    }
}
