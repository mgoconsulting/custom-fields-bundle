<?php

namespace Tests\Form;

use Mgo\CustomFieldsBundle\Form\CustomFieldsFormService;
use PHPUnit\Framework\TestCase;

class CustomFieldsFormServiceTest extends TestCase
{
    public function testEmptyConfig()
    {
        $service = $this->getFormService([]);
        $form = $this->getMockForm('SomeEntityClass');
        $form->expects($this->never())->method('add');

        $service->addCustomFields($form);
    }

    public function testEmptyConfigNoEntity()
    {
        $service = $this->getFormService([]);
        $form = $this->getMockForm();
        $form->expects($this->never())->method('add');

        $service->addCustomFields($form, 'AnEntityClass');
    }

    public function testEmptyConfigNothing()
    {
        $service = $this->getFormService([]);
        $form = $this->getMockForm();
        $form->expects($this->never())->method('add');

        $this->expectException(\LogicException::class);
        $service->addCustomFields($form);
    }

    public function testEmptyConfigBoth()
    {
        $service = $this->getFormService([]);
        $form = $this->getMockForm('EntityClass1');
        $form->expects($this->never())->method('add');

        $this->expectException(\LogicException::class);
        $service->addCustomFields($form, 'EntityClass2');
    }

    public function testBasicConfig()
    {
        $config = [
            'EntityClassX' => [
                'fieldOfX' => [
                    'type' => '...\TextType',
                ],
            ],
            'EntityClassB' => [
                'fieldR' => [
                    'type' => '...\TextType',
                ],
                'fieldS' => [
                    'type' => '...\DateType',
                ],
            ],
        ];
        $service = $this->getFormService($config);
        $form = $this->getMockForm('EntityClassB');
        $form->expects($this->exactly(2))->method('add');

        $service->addCustomFields($form);
    }

    private function getMockForm($formEntityClass = null)
    {
        $mock = $this->getMockBuilder('Symfony\Component\Form\FormBuilderInterface')
            ->getMock()
        ;
        $mockConfig = $this->getMockBuilder('dummy\FormBuilder')
            ->disableAutoload()
            ->setMethods(['getOption'])
            ->getMock()
        ;
        $mockConfig->expects($this->any())->method('getOption')
            ->with($this->equalTo('data_class'))
            ->will($this->returnValue($formEntityClass))
        ;

        $mock->expects($this->any())->method('getFormConfig')
            ->will($this->returnValue($mockConfig))
        ;

        return $mock;
    }

    private function getFormService(array $config)
    {
        $em = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $mr = $this->getMockBuilder('Doctrine\Persistence\ManagerRegistry')->getMock();

        return new CustomFieldsFormService($config, $mr);
    }
}
